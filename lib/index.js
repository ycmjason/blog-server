const express = require('express');
const bodyParser = require('body-parser');
const ip = require('address').ip();
const app = express();

const issueId = (() => {
  let lastId = 0;
  return () => lastId++;
})();

const createPost = ({ title, content }) => ({
  id: issueId(),
  title,
  content,
  created: new Date(),
});

const posts = [
  {
    id: issueId(),
    title: 'Hello world',
    content: 'Welcome to my blog!',
    created: new Date('December 01, 2018'),
  },
  {
    id: issueId(),
    title: 'Lovely weather in Manchester!',
    content: 'Wow! There is sun in the sky!',
    created: new Date('December 02, 2018'),
  },
  {
    id: issueId(),
    title: 'Elysium is awesome',
    content: 'Nice elysium. I love elysium.',
    created: new Date('December 03, 2018'),
  },
];

app.use(require('cors')());
app.use(bodyParser.json());

app.get('/posts/:id', (req, res) => {
  const { id } = req.params;
  if (!(id in posts)) {
    res.status(404);
    res.json({
      error: 'Post Not Found',
      code: 404,
    });

    return;
  }

  res.status(200);
  res.json(posts[id]);
  return;
});

app.get('/posts', (req, res) => {
  res.status(200);
  res.json(posts);
  return;
});

app.post('/posts', (req, res) => {
  const { title, content } = req.body;
  if (req.query.secret !== process.env.SECRET) {
    res.status(401);
    res.json({
      error: 'Unauthorized. Please provide correct secret.',
      code: 401,
    });

    return;
  }

  if (!title || !content) {
    res.status(400);
    res.json({
      error: 'Please provide a json with keys `title` and `content`. It should be sent with application/json too',
      code: 400,
    });

    return;
  }

  // add new post to "database"
  posts.push(createPost({ title, content }));

  res.status(201);
  res.json({
    success: 'Created',
    code: 201,
  });

  return;
});

const PORT = process.env.PORT || 8123;
app.listen(PORT, () => console.log(`  http://${ip}:${PORT}
            or
  http://localhost:${PORT}`));
