# in-memory blog server

## Setup
```
$ npm install
$ SECRET=YOUR_SECRET npm start
```

## API

### Retrieving all posts

```
GET /posts
```

**Response**

```json
[
  {
    "id": 0,
      "title": "Hello world",
      "content": "Welcome to my blog!",
      "created": "2018-12-01T00:00:00.000Z"
  },
  {
    "id": 1,
    "title": "Lovely weather in Manchester!",
    "content": "Wow! There is sun in the sky!",
    "created": "2018-12-02T00:00:00.000Z"
  },
  {
    "id": 2,
    "title": "Elysium is awesome",
    "content": "Nice elysium. I love elysium.",
    "created": "2018-12-03T00:00:00.000Z"
  }
]
```

### Retrieving a single post

```
GET /posts/:id
```

**Response**

```json
{
  "id": 1,
  "title": "Lovely weather in Manchester!",
  "content": "Wow! There is sun in the sky!",
  "created": "2018-12-02T00:00:00.000Z"
}
```

### Create new blog post

```
POST /posts
```

**Parameters**

- `secret`
  - **required**
  - `String`
  - The secret to authorise yourself
- `title`
  - **required**
  - `String`
  - The title of the post
- `content`
  - **required**
  - `String`
  - The content of the post

**Response**

```json
{
    "success": "Created",
    "code": 201
}
```

If the secret is not correct:

```json
{
    "error": "Unauthorized. Please provide correct secret.",
    "code": 401
}
```

## Author
Jason Yu
